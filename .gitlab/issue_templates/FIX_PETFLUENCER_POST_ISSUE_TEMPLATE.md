## Description

The Good Dogs Project recently published a blog post about how to make your dog famous. The content was reviewed and edited before publication, but one of their readers pointed out a few issues in the post. Your job is to fix the errors the reader found.

## Your tasks

There are three tasks you must complete. You'll complete your work in the `/content/blog/make-your-dog-a-star.md` file.

### Add bold text

- [ ] Bold the term "petfluencers" in the second paragraph of the introduction.

### Fix typo in link text

- [ ] Fix the typo in the link text in the **Write great captions** section.

### Add a bulleted list

- [ ] Convert the information in the **Write great captions** section into an unordered bulleted list. Use dashes (-) or asterisks (*) in front of each item.

## Before you open a merge request

- [ ] Review the issue to make sure you completed all tasks.
- [ ] Make sure that you committed your changes your branch, not `main`.